# Actions In Context
## Abstract
**Exploring the Influence of Context Information on Action Recognition: a fMRI Multivariate Pattern Analysis Approach**

Studying how humans perceive and process other agents’ actions is a complex endeavour. In real life, actions are embedded in meaningful contexts, which generally offer a broad spectrum of information adding to kinematic, movement information. However, the role of context is virtually absent in neuroscientific approaches investigating this ‘Social Cognition’.
In a novel paradigm, without the necessity of relying on arbitrary theoretical constructs often used in psychology (such as ‘intentions’), we are specifically targeting how visually presented scene information is processed when perceiving an action. In a tailored experiment, participants view point light display videos of everyday actions (e.g. ‘brushing teeth’ or ‘chopping’), which are superimposed on context images (e.g. of a ‘bathroom’ or a kitchen’). 

![Graphical_Abstract](./Experiment/doc_imgs/TaskDesign_LQ.png)


Per trial, the presented context image is either congruent, incongruent or neutral regarding the simultaneously presented action. During the presentation of those motion-captured action stimuli, event-related brain activity will be recorded with functional Magnet Resonance Tomography (fMRI) and subsequently analysed using ‘The Decoding Toolbox’[1]. Multivariate Pattern Analysis is specifically suitable for our paradigm, as we are able to compare patterns of evoked brain activity when participants are perceiving an action in the right vs wrong context, rather than mass-comparing gross BOLD activation in narrow regions of interest.

_References:_

[1] **Hebart, M. N., Görgen, K., & Haynes, J. D. (2015).** _The Decoding Toolbox (TDT): a versatile software package for multivariate analyses of functional imaging data._ Frontiers in Neuroinformatics, 8, 88.

---
## Repository Content
Repository *(in progress)* contains following sub repositories:

* **Motion_Capture_Data_Processing**

    Set of functions and scripts to create animations from 3D-Motion capture data

* **Behavioural_Data_Analysis**

    Scripts and Functions for the Data Analysis of Behavioural Data

* **Experiment**

    Scripts, Functions and Data for running the fMRI- and behavioural experiments
---

