#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ###################################################################################
from psychopy import core, visual, event, clock
import csv
import numpy as np
import os
import sys
import glob
import random
from PIL import Image
import re


def numericalSort(value):
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


def getString(window, question="Type: text followed by return"):

    string = ""
    InKeys = ""
    shift_flag = False

    while True:
        message = visual.TextStim(
            window, alignHoriz="center", text=question+string, color='black')
        message.draw()
        window.flip()
        InKeys = event.waitKeys()
        if InKeys[0] == 'return':
            return string
        elif InKeys[0] == 'backspace':
            string = string[:-1]
        elif InKeys[0] == 'space':
            string += ' '
        elif InKeys[0] in ['lshift', 'rshift']:
            shift_flag = True
        else:
            if len(InKeys[0]) == 1:
                if shift_flag:
                    string += chr(ord(InKeys[0])-ord(' '))
                    shift_flag = False
                else:
                    string += InKeys[0]


def showText(window, inputText="Text", col='white', he=1):
    message = visual.TextStim(window, alignHoriz='center',
                              alignVert='center', text=inputText, color=col, height=he)
    message.draw()
    window.flip()


def dotVideo1(window, pngFolder, backgroundimage, frameTime, delayTime, dotFieldSize, numDots=400, dotSiz=10):

    image1 = visual.ImageStim(window, image=backgroundimage, pos=(
        0, 0), units='height', size=(1, 1))

    path = pngFolder + "/*.*"

    files = sorted(glob.glob(path), key=numericalSort)

    image1.draw()
    window.flip()
    core.wait(delayTime)  # define delay time here!

    #ds = visual.DotStim(window, fieldSize=dotFieldSize, dotSize=dotSiz, fieldShape='sqr', speed=0, dotLife=10, nDots=numDots, coherence=0.8, color=[-1, -1, -1], colorSpace='rgb', opacity=1, contrast=1, noiseDots='direction')
    ds = visual.DotStim(window, fieldSize=(31.0), dotSize=dotSiz, fieldShape='sqr', speed=0, dotLife=10, nDots=numDots, coherence=0.5,
                        color=[-1, -1, -1], colorSpace='rgb', opacity=1, contrast=1, signalDots='different', dir=180, noiseDots='direction')
    #ds = visual.DotStim(window, fieldSize=(9,25), dotSize=dotSiz, speed=0, dotLife=20, nDots=numDots, coherence=0.8, color=[-1, -1, -1], colorSpace='rgb', opacity=1, contrast=1, signalDots='different', noiseDots='direction = 180')

    syncTime = clock.getTime()

    for file in files:
        # calculated by screen refreshing rate (here in case of 35 hz-->28,xxx msec) ADJUST WHEN CHANGING FRAME RATE VIDEO
        syncTime = syncTime + frameTime
        image1.draw()
        visual.ImageStim(window, image=file, pos=(
            0, 0), units='height', size=(1, 1)).draw()
        ds.draw()
        while clock.getTime() < syncTime:
            pass
        window.flip()
        syncTime = clock.getTime()


def dotVideoText(window, pngFolder, backgroundimage, frameTime, inputText="Text", col='black'):
    image1 = visual.ImageStim(window, image=backgroundimage, pos=(
        0, 0), units='height', size=(1, 1))
    message = visual.TextStim(window, alignHoriz='center', alignVert='center', pos=(
        0, -0.9), text=inputText, color=col, units='norm')

    path = pngFolder + "/*.*"
    files = sorted(glob.glob(path), key=numericalSort)

    image1.draw()
    window.flip()

    syncTime = clock.getTime()

    for file in files:
        # calculated by screen refreshing rate (here in case of 35 hz-->28,xxx msec) ADJUST WHEN CHANGING FRAME RATE VIDEO
        syncTime = syncTime + frameTime
        image1.draw()
        visual.ImageStim(window, image=file, pos=(
            0, 0), units='height', size=(1, 1)).draw()
        message.draw()
        while clock.getTime() < syncTime:
            pass
        window.flip()
        syncTime = clock.getTime()


def dotVideo2(window, pngFolder, backgroundimage, frameTime, delayTime, rndFrames, nDots, rndDotsRefresh):
    win = window
    image1 = visual.ImageStim(win, image=backgroundimage, pos=(
        0, 0), units='height', size=(1, 1))

    # create a random dot array with n_dots over t_frames (finite number of differnt dot arrangements)
    n_dots = nDots
    t_frames = rndFrames
    t_fr = []

    for t in range(t_frames):
        dot_xys = []
        for dot in range(n_dots):
            dot_x = random.uniform(-300, 300)
            dot_y = random.uniform(-300, 300)
            dot_xys.append([dot_x, dot_y])
        t_fr.append(dot_xys)

    ds = visual.ElementArrayStim(
        win=win,
        units="pix",
        nElements=n_dots,
        elementTex=None,
        elementMask="circle",
        colors=(1, 1, 1), colorSpace='rgb',
        xys=t_fr[1],
        sizes=10
    )

    path = pngFolder + "/*.*"
    files = glob.glob(path)
    image1.draw()
    win.flip()
    core.wait(delayTime)

    Timer = core.Clock()

    offsetTime = Timer.getTime() + delayTime
    syncTime = Timer.getTime()
    x = 0
    while Timer.getTime() < offsetTime:
        syncTime = syncTime + frameTime
        if (x % rndDotsRefresh == 0):
            t = random.randint(0, t_frames)
        x = x+1
        image1.draw()
        ds.sizes = 10
        ds.colors = (-1, -1, -1)
        ds.xys = t_fr[t]
        ds.draw()
        ds.sizes = 8
        ds.colors = (1, 1, 1)
        ds.draw()
        win.flip()
        while Timer.getTime() < syncTime:
            pass

    x = 0
    for file in files:
        syncTime = syncTime + frameTime
        if (x % rndDotsRefresh == 0):
            t = random.randint(0, t_frames)
        x = x+1
        image1.draw()
        ds.sizes = 10
        ds.colors = (-1, -1, -1)
        ds.xys = t_fr[t]
        ds.draw()
        ds.sizes = 8
        ds.colors = (1, 1, 1)
        ds.draw()
        visual.ImageStim(win, image=file, pos=(
            0, 0), units='height', size=(1, 1)).draw()
        win.flip()
        while Timer.getTime() < syncTime:
            pass


def convertToPNG(path):
    files = glob.glob(path)
    for file in files:
        print(file)
        img = Image.open(file)  # image path and name
        img = img.convert("RGBA")
        datas = img.getdata()
        newData = []
        for item in datas:
            if item[0] > 160 and item[1] > 160 and item[2] > 160:
                newData.append((255, 255, 255, 0))
            elif item[0] > 160 and item[1] < 160 and item[2] < 160:
                newData.append((255, 255, 255, 255))
            else:
                newData.append(item)
        img.putdata(newData)
        img.save(file, "PNG")  # converted Image name
    print('Done')


def dotVideo_noNoise(window, pngFolder, backgroundimage, frameTime, delayTime):

    image1 = visual.ImageStim(window, image=backgroundimage, pos=(
        0, 0), units='height', size=(1, 1))

    path = pngFolder + "/*.*"

    files = sorted(glob.glob(path), key=numericalSort)

    image1.draw()
    window.flip()
    core.wait(delayTime)

    syncTime = clock.getTime()

    for file in files:
        # calculated by screen refreshing rate (here in case of 35 hz-->28,xxx msec) ADJUST WHEN CHANGING FRAME RATE VIDEO
        syncTime = syncTime + frameTime
        image1.draw()
        visual.ImageStim(window, image=file, pos=(
            0, 0), units='height', size=(1, 1)).draw()
        while clock.getTime() < syncTime:
            pass
        window.flip()
        syncTime = clock.getTime()


def convLetter2Number(Letter):
    return {
        'a': 4,
        'A': 4,
        'b': 3,
        'B': 3,
        'c': 2,
        'C': 2,
        'd': 1,
        'D': 1,
        'e': 5,
        'E': 5,
    }[Letter]


def wait_push_button(time, bb):
    core.wait(time)
    if bb:
        bb.waitButtons()
    else:
        response = event.waitKeys()


def get_response(responseStart, correct, wrong_same_context, max_time, bb):
    
    b_quit = False
    if bb:
        response = bb.waitButtons(
            buttonList=['a', 'A', 'b', 'B', 'c', 'C', 'd', 'D'], maxWait=max_time)
        response = convLetter2Number(response[0])
    else:
        response = event.waitKeys(
            keyList=['1', '2', '3', '4', 'q'], maxWait=max_time)

    reactionTime = clock.getTime()-responseStart
    # Change into milliseconds
    reactionTime = int(round(reactionTime * 1000))
    if response:  # handles no response/timeout
        if response[0] == str(correct):
            res = 2
        elif response[0] == str(wrong_same_context):
            res = 1
        elif response[0] == 'q':
            res = 5
            b_quit = True
        else:
            res = 0
    else:
        res = 5


    return res, reactionTime, b_quit


def get_ratings(win, baseDir, ParticipantNumber, mode, language):

    def get_rating(win, image, text):
        ratingScale = visual.RatingScale(
            win, scale=text, low=1, high=6, precision=1, textColor='red', lineColor='red')
        while ratingScale.noResponse:
            image.draw()
            ratingScale.draw()
            win.flip()
            rating = ratingScale.getRating()
        return rating

    rating_path = os.path.join(baseDir, 'Results', str(
        mode)+'_Participant_'+str(ParticipantNumber)+'_Rating.csv')

    if language == 'nl':
        questions = [
            'Hoe waarschijnlijk is het dat je in deze ruimte je haaren wast?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte hakken?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte wist?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte schroeft?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte raspt?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte timmert?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte op een whitebord schrijft?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte je tanden poetst?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte punten slijpt?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte een wijnfles opent?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte verft?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte je gezicht wast?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte je haar kamt?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte iets vastniet?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte een bierflesje opent?\n\n\n',
            'Hoe waarschijnlijk is het dat je in deze ruimte zaagt?\n\n\n'
        ]
    else:
        questions = [
            'How likely is it, that you are shampooing in this room?\n\n\n',
            'How likely is it, that you are chopping in this room?\n\n\n',
            'How likely is it, that you are erasing in this room?\n\n\n',
            'How likely is it, that you are screwing in this room?\n\n\n',
            'How likely is it, that you are grating in this room?\n\n\n',
            'How likely is it, that you are hammering in this room?\n\n\n',
            'How likely is it, that you are writing on a whiteboard in this room?\n\n\n',
            'How likely is it, that you are brushing teeth in this room?\n\n\n',
            'How likely is it, that you are sharpening a pencil in this room?\n\n\n',
            'How likely is it, that you are opening a bottle in this room?\n\n\n',
            'How likely is it, that you are painting in this room?\n\n\n',
            'How likely is it, that you are washing your face in this room?\n\n\n',
            'How likely is it, that you are combing your hair in this room?\n\n\n',
            'How likely is it, that you are stapeling in this room?\n\n\n',
            'How likely is it, that you are opening beer in this room?\n\n\n',
            'How likely is it, that you are sawing in this room?\n\n\n'
        ]

    presImages = ["Bathroom1.png", "Kitchen4.png",
                  "Office3.png", "Garage3.png"]

    RatingFile = open(rating_path, 'w+')
    RatingFile.write('Image;RatingShampooing;RatingChopping;RatingErasing;RatingScrewing;RatingGrating;RatingHammering;RatingWritingWall;RatingBrushingTeeth;RatingSharpeningPencil;RatingOpeningBottle;RatingPainting;RatingWashingFace;RatingCombing;RatingStapling;RatingOpeningBeer;RatingSawing\n')
    RatingFile.close()

    for v in range(0, len(presImages)):

        pic = os.path.join(baseDir, "Images", ' '.join(presImages[v]))
        image = visual.ImageStim(win, pic)
        rating = []
        for ac in range(0, len(questions)):
            rating.append(str(get_rating(win, image, questions[ac])))

        # Write to file
        entry = [' '.join(presImages[v])] + rating

        RatingFile = open(rating_path, 'a')
        writerRat = csv.writer(RatingFile, delimiter=';', lineterminator='\n')
        writerRat.writerow(entry)
        RatingFile.close()


# Pause and interaction with 'scanner'
def block_break(win,run,triggerPulse):
    if run == 3:    # On Last Run
        showText(win, "Dat was het! Blijf nog even stil liggen\n dan doen we nog enkele metingen")
        event.waitKeys(keyList = ['space', 'escape'])
        protocolFinished = 0
        while protocolFinished == 0:
            breakInput = event.waitKeys(maxWait = 10, keyList = ['space', 'escape'])
            if breakInput:
                if breakInput [0] == 'escape':
                    break        
            else:
                protocolFinished = 1
                print protocolFinished
                showText(win, "Dat was het!")
                core.wait(10)
    else: # Other breaks
        showText(win, "Een korte pauze...")
        receiveTrigger = 1
        while receiveTrigger == 1:
            input = event.waitKeys(maxWait = 5, keyList = [triggerPulse, 'escape'])
            if input:
                if input[0] == '5':
                    print input
                    receiveTrigger = 1
            else:
                receiveTrigger = 0