#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ###################################################################################
# Library Imports.
# ####################################################################################
import csv
import os
import sys
import datetime
import pandas as pd  # panda library for reading and writing csv
import fun  # custom fuctions, see other .py
import random
import numpy as np
from psychopy import core, clock, visual, event
import serial

#from psychopy.data import ExperimentHandler
#from psychopy.hardware.emulator import launchScan

b_bb = False
# To use the buttonbox instead of keyboard use b_bb = True:
if b_bb:
    from rusocsci import buttonbox
    # make a buttonbox
    bb = buttonbox.Buttonbox()
else:
    bb = None

# setting up exception to get out of loop


class GetOutOfLoop(Exception):
    pass

# ####################################################################################
# Script Setup--
# ####################################################################################


# setup mode
mode = 'pilot'  # 'scan' or 'pilot' -->to have this in output file name
responseTime = 7

# ####################################################################################
# Directory & Log-File
# ####################################################################################

# gets the directory from where the script was started from Operating Sytem-->flexible
baseDir = sys.path[0]

# setup Log-File
if os.path.isfile('log.csv'):
    print('Logfile found')
    LogData = pd.read_csv('log.csv', sep=';')
    emptyLog = False
    # to control for the assignment in block-randomisation group!
    # --> if more completed sets in 'starting with block normal'
    # --> then 'stretched' assigned, 0 = normal 1 = stretched
    completedExperiments = LogData[LogData['Completed'] == 1]
    if len(completedExperiments[completedExperiments['SequenceMode'] == 1]) >= len(completedExperiments[completedExperiments['SequenceMode'] == 0]):
        seqMode = 0
    else:
        seqMode = 1
else:
    print('no Logfile found. I am creating a new one')
    logHeader = open('log.csv', 'w+')
    logHeader.write(
        'Mode;SequenceMode;Participant;DateTime;DateTimeFinished;Experimenter;Completed\n')
    logHeader.close()
    emptyLog = True
    seqMode = 0

# ####################################################################################
# Data import and arrangement
# ####################################################################################

# Read trial info from file in one block as an array for better performance
# Array size and column names are taken from the csv file automatically
# --> careful with conditions in that file!!!
trialData = pd.read_csv('stimuli_behavioural_dutch.csv', sep=',')

# Slicing Trials from csv and randomizing the sequences within the blocks

# correct here for the right indexed numbers!
training1 = trialData[0:6].reindex(np.random.permutation(trialData[0:6].index))
training2 = trialData[6:12].reindex(
    np.random.permutation(trialData[6:12].index))
Block1 = trialData[12:84].reindex(
    np.random.permutation(trialData[12:84].index))
Block2 = trialData[84:156].reindex(
    np.random.permutation(trialData[84:156].index))
Block3 = trialData[156:228].reindex(
    np.random.permutation(trialData[156:228].index))
Block4 = trialData[228:300].reindex(
    np.random.permutation(trialData[228:300].index))

trialData = []
trialData = pd.concat([training1, training2, Block1, Block2, Block3, Block4])

# randomize Video presentation sequence
presVids =\
    [[0], ["washingface"], ['Het gezicht wassen']],\
    [[1], ["shampooing"], ['Haren wassen']],\
    [[2], ["brushingteeth"], ['De tanden poetsen']],\
    [[3], ["chopping"], ['Hakken']],\
    [[4], ["erasing"], ['Wissen']],\
    [[5], ["grating"], ['Raspen']],\
    [[6], ["hammering"], ['Hameren']],\
    [[7], ["openingbottle"], ['Een wijnfles openen']],\
    [[8], ["painting"], ['Verven']],\
    [[9], ["writingwall"], ['Schrijven (whiteboard)']],\
    [[10], ["sharpeningpencil"], ['Puntenslijpen']],\
    [[11], ["screwing"], ['Scroeven']]
# [[11],["shaving"],['Shaving']]   #hier ist shaving noch mit drin, aber das dürfte es nicht, kontrollieren
VidOrder = range(0, len(presVids))
random.shuffle(VidOrder)

# ####################################################################################
# Screen Setup
# ####################################################################################
bkgColor = 'darkgrey'
txtColor = 'black'
# win = visual.Window(size=(900, 700),color=bkgColor, colorSpace='rgb', fullscr=False) #change to fullscreen when experiment done
winSize = 900
win = visual.Window([winSize, winSize], monitor="testMonitor",
                    units="cm", color=bkgColor, colorSpace='rgb', fullscr=False)
dotSize = 0.013*winSize

# ####################################################################################
# Session Setup
# ####################################################################################

# Experimenter
Experimenter = fun.getString(win, 'Experimenter: \n \n')

# Participant
# if no integer -->message and loop, also control for no already used!--> warning
while True:
    ParticipantNumber = fun.getString(win, 'Participant Number: \n \n')
    try:
        ParticipantNumber = int(ParticipantNumber)
        if emptyLog == False:
            if ParticipantNumber in LogData['Participant'].values:
                fun.showText(win, 'Participant number '+str(ParticipantNumber)+' was found in the Log! Do you want to overwrite the existing data for this participant? \n (Y/N) \n \n Entry: \n' +
                             str(LogData.loc[LogData['Participant'] == ParticipantNumber]), txtColor)
                response = event.waitKeys(keyList=['y', 'n'])
                if response[0] == 'y':
                    break
            else:
                break
        else:
            break
    except ValueError:
        mes = visual.TextStim(win, alignHoriz="center",
                              text='Please enter a number!', color='white')
        mes.draw()
        win.flip()
        core.wait(3)

# Date and Time (from OS)
DateTime = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")

completed = 0

# ####################################################################################
# Experiment Loop
# ####################################################################################

ResFileName = os.path.join(baseDir, 'Results', str(
    mode)+'_Participant_'+str(ParticipantNumber)+'.csv')
ResFile = open(ResFileName, 'w+')
ResFile.write('Condition;Action;Task;VideoFile;NumberVideoFile;ImageFile;NumberImageFile;TrialNum;VideoConfiguration;VideoMarker;VideoSize;VideoMirror;Response;ReactionTime;\n')

try:
    for index in range(0, len(trialData)):

        # pre-load image and video
        pic = os.path.join(baseDir, "Images",
                           trialData.iloc[index]['ImageFile'])
        vid = os.path.join(
            baseDir, "Videos", trialData.iloc[index]['VideoFile'], trialData.iloc[index]['Video'])

        # trialData.iloc[[index]]['Video']
        picture = visual.ImageStim(win, pic, pos=(0.0, 0.0))

        if index == 0:  # Training 1a
            fun.showText(win, """Welkom!\nIn dit experiment krijg je verschillende filmpjes te zien. Ieder filmpje laat een actie zien.
            \n\nNae afloop zie je vier beschrijvingen van acties. Het is jouw taak om de juiste beschrijving te kiezen, en de bijbehorende knop (1,2,3,4) in te drukken.\n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
            fun.wait_push_button(2, bb)
            # bb.waitButtons()
            # fun.showText(win,"""Welcome!\nIn the following experiment you are going to see different videos. In every video, an action is shown.
            # \n\nAfterwards, a selection of action labels will be presented to you, to which you reply by button presses (1,2,3,4). Your task is to recognise the shown action as precisely as possible and press the corresponding button. \n\nPress any key to continue!""",txtColor,0.7)
            fun.showText(win, """\n\nGedurende het experiment zul je verschillende houdingen aannemen. In de 'wijd' blokken pak je twee joysticks links en rechts vast, zodra je een kruisje op het scherm ziet. Je houdt je handen wijd gedurende het kijken naar het filmpje. Om antwoord te geven breng je je handen natuurlijk weer naar het midden.""", txtColor, 0.7)
            #fun.showText(win,"""\n\nThere are two different positions you will be adopting while watching the video. In blocks 'streched' you are going to grasp the two joysticks on your left, and right respectively, as soon as you see the fixation cross and keep your hands there while the video is shown. To react to the answer options, you come back to a normal position.""",txtColor,0.7)
            fun.wait_push_button(2, bb)
            fun.showText(
                win, """\n\nIn de 'smal' blokken houd je handen voor je op tafel. \n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
            #fun.showText(win,"""\n\nIn block 'smal', your hands will remain in a normal position, your fingers resting the button box. \n\nPress any key to continue!""",txtColor,0.7)
            fun.wait_push_button(2, bb)
            fun.showText(
                win, """Nu een blok 'smal': houd je handen voor je op tafel gedurende het fimplje. \n\nDruk on een toets om door te gaan.""", txtColor, 0.7)
            #fun.showText(win,"""Now, an example for block 'regular': please keep your hands rested on the button boxes while watching the video. \n\nPress any key to continue!""",txtColor,0.7)
            fun.wait_push_button(2, bb)
        elif index == 6:  # Training 1b
            fun.showText(win, """Nu een voorbeeld van een 'wijd' blok. Pak aub beide joysticks vast zodra het kruisje op het beeld verschijnt, en houd je handen wijd gedurende het filmpje. \n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
            #fun.showText(win,"""Now, an example for the block 'stretched': please grasp the joysticks when you see the fixation cross and remain in this position for the whole duration the video is presented. \n\nPress any key to continue!""",txtColor,0.7)
            fun.wait_push_button(2, bb)
        elif index == 12:  # presenting all actions
            #fun.showText(win,'The training sequence is finished.\n\nPress any key to continue!',txtColor,0.7)
            fun.showText(
                win, 'Dat was de training. \n\n Druk op een toets om door te gaan.', txtColor, 0.7)
            fun.wait_push_button(2, bb)
            core.wait(2)
            #fun.showText(win,"""You are now watching videos of all actions with the right label shown at the bottom of the screen. Please pay close attention, every action will only be shown once. \n\nPress any key to continue!""",txtColor,0.7)
            fun.showText(win, """Je krijgt nu van elke actie een voorbeeld te zien, met de juiste beschrijving onder in beeld. Let goed op, elke actie komt maar een keer langs. \n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
            fun.wait_push_button(0, bb)
            core.wait(3)
            fun.showText(win, '+', txtColor)
            for v in range(0, len(presVids)):
                ind = VidOrder[v]
                fun.dotVideoText(win, os.path.join(baseDir, "Videos", presVids[ind][1][0], "M1_S3_I0"), os.path.join(
                    baseDir, "Images", "Image_Background.png"), 0.017*2, presVids[ind][2][0], txtColor)
                fun.showText(win, '+', txtColor)
            # Block 1
            if seqMode == 0:
                #fun.showText(win,'Block 1: stretched \n\nStretch your arms out. \n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 1: Wijd. \n\nSpreid je armen. \n\nDruk op een toets om door te gaan.', txtColor)
                fun.wait_push_button(5, bb)
            else:
                #fun.showText(win,'Block 1: regular \en\nPut your hands close to the buttons.\n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 1: Smal. \n\nLeg je handen voor je op tafel. \n\nDruk op een toets om door te gaan. ', txtColor)
                fun.wait_push_button(5, bb)
        elif index == 85:
            # Block 2
            if seqMode == 0:
                #fun.showText(win,'Block 2: Smal \n\nPut your hands close to the buttons.\n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 2: Smal. \n\nLeg je handen voor je op tafel. \n\nDruk op een toets om door te gaan. ', txtColor)
                fun.wait_push_button(5, bb)
            else:
                #fun.showText(win,'Block 2: stretched \n\nStretch your arms out... \n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 2: Wijd. \n\nSpreid je armen. \n\nDruk op een toets om door te gaan. ', txtColor)
                fun.wait_push_button(5, bb)
        elif index == 157:
            # Block 3
            if seqMode == 0:
                #fun.showText(win,'Block 3: stretched \n\nStretch your arms out... \n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 3: Wijd. \n\nSpreid je armen. \n\nDruk op een toets om door te gaan. ', txtColor)
                fun.wait_push_button(5, bb)
            else:
                #fun.showText(win,'Block 3: regular \n\nPut your hands close to the buttons.\n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 3: Smal. \n\nLeg je handen voor je op tafel. \n\nDruk op een toets om door te gaan.', txtColor)
                fun.wait_push_button(5, bb)
        elif index == 229:
            # Block 4
            if seqMode == 0:
                #fun.showText(win,'Block 4: regular \n\nPut your hands close to the buttons.\n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 4: Smal. \n\nLeg je handen voor je op tafel. \n\nDruk op een toets om door te gaan.', txtColor)
                fun.wait_push_button(5, bb)
            else:
                #fun.showText(win,'Block 4: stretched \n\nStretch your arms out... \n\nPress any key to continue!',txtColor)
                fun.showText(
                    win, 'Blok 4: Wijd. \n\nSpreid je armen. \n\nDruk op een toets om door te gaan. ', txtColor)
                fun.wait_push_button(5, bb)

        # Show fixation cross
        fun.showText(win, '+', txtColor)
        response = event.waitKeys(keyList=['q'], maxWait=0.5)
        if response:
            if response[0] == 'q':
                completed = 0
                raise GetOutOfLoop

        # show video
        fun.dotVideo1(win, vid, pic, 0.017*2, .5, 100, 700, dotSize)
        fun.showText(win, '', txtColor)

        reactionTime = 0
        responseStart = clock.getTime()
        fun.showText(win, (trialData.iloc[index]['Question']+'\n \n 1: ' + trialData.iloc[index]['Answer1']+'\n 2: '+trialData.iloc[index]
                           ['Answer2']+'\n 3: '+trialData.iloc[index]['Answer3']+'\n 4: '+trialData.iloc[index]['Answer4']), txtColor)
        [response, reactionTime, b_quit]  = fun.get_response(responseStart, trialData.iloc[index]['Correct'], trialData.iloc[index]['WrongSameContext'], trialData.iloc[index]['WrongSameContext'], bb)

        # RESULTSENTRY
        ResEntry = [trialData.iloc[index]['Condition'],
                    trialData.iloc[index]['Context'],
                    trialData.iloc[index]['Task'],
                    trialData.iloc[index]['VideoFile'],
                    trialData.iloc[index]['NoVideoFile'],
                    trialData.iloc[index]['ImageFile'],
                    trialData.iloc[index]['NoImageFile'],
                    trialData.iloc[index]['TrialNum'],
                    trialData.iloc[index]['Video'],
                    trialData.iloc[index]['Video_Marker'],
                    trialData.iloc[index]['Video_Size'],
                    trialData.iloc[index]['Video_Mirror'],
                    response,
                    reactionTime]

        writerR = csv.writer(ResFile, delimiter=';', lineterminator='\n')
        writerR.writerow(ResEntry)

        if b_quit:
            completed = 0
            raise GetOutOfLoop

        if index == len(trialData)-1:
            completed = 1
except GetOutOfLoop:
    pass
    print('\n\nExperiment aborted.')

ResFile.close

# Date and Time (from OS)
DateTimeFinished = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
logEntry = [mode, seqMode, ParticipantNumber, DateTime,
            DateTimeFinished, Experimenter, completed]
logFile = open('log.csv', 'a')
writerL = csv.writer(logFile, delimiter=';', lineterminator='\n')
writerL.writerow(logEntry)
logFile.close

# Rating Part
if completed:
    fun.get_ratings(win, baseDir, ParticipantNumber, mode, 'nl')

fun.showText(win, 'Thank you a lot for your participation.', txtColor)

core.wait(5)
core.quit()
