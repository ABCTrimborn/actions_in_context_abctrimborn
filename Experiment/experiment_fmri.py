#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ###################################################################################
# Library Imports.
# ####################################################################################
import csv
import io
import os
import time
import random
import serial
import sys
import datetime
import pyglet
import pandas as pd  # panda library for reading and writing csv
import fun  # custom fuctions, see other .py
import random
import numpy as np
from psychopy import core, clock, visual, event, logging, monitors
from psychopy.hardware.emulator import launchScan

# setting up exception to get out of loop
class GetOutOfLoop(Exception):
    pass

# ####################################################################################
# Script Setup--
# ####################################################################################


# setup mode
mode = 'pilot'  # 'scan' or 'pilot' -->to have this in output file name
responseTime = 3
triggerPulse = '5'

# ####################################################################################
# Directory & Log-File
# ####################################################################################

# gets the directory from where the script was started from Operating Sytem-->flexible
baseDir = sys.path[0]
print(baseDir)

# setup Log-File
if os.path.isfile('scan_log.csv'):
    print('Logfile found')
    LogData = pd.read_csv('scan_log.csv', sep=';')
    emptyLog = False

else:
    print('no Logfile found. I am creating a new one')
    logHeader = open('scan_log.csv', 'w+')
    logHeader.write('Participant;DateTime;DateTimeFinished;Experimenter; \n')
    logHeader.close
    emptyLog = True

# retrieving training Data
trainingData = pd.read_csv('stimuli_training_fmri_dutch.csv.csv', sep=',')
trialData = pd.read_csv('stimuli_trials_fmri_dutch.csv.csv', sep=',')

# set up actions for presentation at the beginning:
presVids =\
    [[0], ["grating"], ['raspen']],\
    [[1], ["hammering"], ['hameren']],\
    [[3], ["chopping"], ['snijden']],\
    [[4], ["painting"], ['verven']]

# randomising video order for training
VidOrder = range(0, len(presVids))
random.shuffle(VidOrder)

# ####################################################################################
# Screen Setup
# ####################################################################################
bkgColor = 'darkgrey'
txtColor = 'black'
winSize = 900
win = visual.Window([winSize, winSize], monitor="testMonitor",
                    units="cm", color=bkgColor, colorSpace='rgb', fullscr=False)
dotSize = 0.013*winSize

# ####################################################################################
# Session Setup
# ####################################################################################

# Experimenter
Experimenter = fun.getString(win, 'Experimenter: \n \n')

# initialise the check for participant number
while True:
    ParticipantNumber = fun.getString(win, 'Participant Number: \n \n')
    try:
        ParticipantNumber = int(ParticipantNumber)
        if emptyLog == False:
            if ParticipantNumber in LogData['Participant'].values:
                fun.showText(win, 'Participant number '+str(ParticipantNumber)+' was found in the Log! Do you want to overwrite the existing data for this participant? \n (Y/N) \n \n Entry: \n' +
                             str(LogData.loc[LogData['Participant'] == ParticipantNumber]), txtColor)
                response = event.waitKeys(keyList=['y', 'n'])
                if response[0] == 'y':
                    break
            else:
                break
        else:
            break
    except ValueError:
        mes = visual.TextStim(win, alignHoriz="center",
                              text='Please enter a number!', color='white')
        mes.draw()
        win.flip()
        core.wait(3)

# Date and Time (from OS)
DateTime = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
# print(DateTime)

# set up results file
ResFileName = os.path.join(os.sep, baseDir, "Results", str(
    mode)+"_Participant_"+str(ParticipantNumber)+".csv")
print(ResFileName)
ResFile = open(ResFileName, "w+")

# adjust the appropriate column names here!
ResFile.write('Condition;Context;Action;VideoFile;NoVideoFile;ImageFile;NoImageFile;TrialNum;Video;VideoMarker;VideoSize;VideoMirror;Response;ReactionTime;VideoPresentationTime;startRun;startTrial;startVid;Block\n')


# #################################### Input ####################################

input = fun.getString(win, 'Testing (T) or scanning (S)?')
if input == 't':
    delay = 1
    runMode = 'Test'
    waitMessage = 'waiting for scanner...'
    MR_settings = {
        'TR': 1.500,  # duration (sec) per volume
        'volumes': 210,  # number of whole-brain 3D volumes / frames                                         _________________ANPASSEN
        # character to use as the sync timing event; assumed to come at start of a volume
        'sync': 'equal',
        # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
        'skip': 0,
    }
else:
    runMode = 'Scan'
    waitMessage = ''
    MR_settings = {
        'TR': 1.500,  # duration (sec) per volume
        'volumes': 210,  # number of whole-brain 3D volumes / frames
        'sync': '5',  # character to use as the sync timing event; assumed to come at start of a volume -->cannot be 5 even if default, as otheriwse used
        # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
        'skip': 5,
    }

# initialising Training/Experiment gui:

input = fun.getString(win, 'Do you want to do a training? (y/n)')
if input == 'y':
    # ################################ Training ###########################################
    # ### randomisation of csv lists
    trainingData = trainingData.reindex(np.random.permutation(
        trainingData.index))  # reindex --> shuffle

    try:
        for index in range(0, len(trainingData)):
            pic = os.path.join(baseDir, "Images",
                               trainingData.iloc[index]['ImageFile'])

            vid = os.path.join(
                baseDir, "Videos", trainingData.iloc[index]['VideoFile'], trialData.iloc[index]['Video'])

            picture = visual.ImageStim(win, pic, pos=(0.0, 0.0))
            startRun = clock.getTime()
            if index == 0:  # Training 1a
                fun.showText(win, """Welkom!\nIn het volgende experiment krijg videos van acties te zien.
                \n\nDaarna verschijnen beschrijvingen van acties. Kies de beschrijving die past bij de vertoonde actie door de bijbehorende knop in te drukken (1, 2, 3).\n\nDruk op een knop om door te gaan!""", txtColor, 0.7)
                core.wait(2)
                response = event.waitKeys(keyList=["1", "2", "3", "0"])
            elif index == 6:  # Training 1b
                fun.showText(
                    win, """Nu nog een voorbeeld van een blok. \n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
                core.wait(2)
                response = event.waitKeys(keyList=["1", "2", "3", "0"])
            startTrial = clock.getTime()  
            message = visual.TextStim(
                win, text='+', alignHoriz='center', alignVert='center', color='white', height=1)
            message.draw()
            win.flip()
            startVid = clock.getTime()
            response = event.waitKeys(keyList=['q'], maxWait=1.0)

            if response:
                if response[0] == 'q':
                    completed = 0
                    raise GetOutOfLoop

            fun.dotVideo1(win, vid, pic, 0.04, 1.5, 100, 400, dotSize)

            # Greyscreen before answer options
            win.flip()
            core.wait(1.5)

            ##########
            fun.showText(win, '', txtColor)
            responseStart = clock.getTime()
            VideoPresentationTime = responseStart - startVid
            fun.showText(win, (trainingData.iloc[index]['Question']+'\n \n 1: ' + trainingData.iloc[index]['Answer1'] +
                               ' 2: '+trainingData.iloc[index]['Answer2']+' 3: '+trainingData.iloc[index]['Answer3']), txtColor)

            response = event.waitKeys(
                keyList=['1', '2', '3', '0', 'q'], maxWait=responseTime)
            reactionTime = clock.getTime()-responseStart

            # conversion into milliseconds
            reactionTime = int(round(reactionTime * 1000))
            # entering results
            ResEntry = [trainingData.iloc[index]['Condition'], 
                        trainingData.iloc[index]['Context'], 
                        trainingData.iloc[index]['Action'], 
                        trainingData.iloc[index]['VideoFile'], 
                        trainingData.iloc[index]['NoVideoFile'], 
                        trainingData.iloc[index]['ImageFile'], 
                        trainingData.iloc[index]['NoImageFile'],
                        trainingData.iloc[index]['TrialNum'], 
                        trainingData.iloc[index]['Video'], 
                        trainingData.iloc[index]['Video_Marker'], 
                        trainingData.iloc[index]['Video_Size'], 
                        trainingData.iloc[index]['Video_Mirror'], 
                        response, 
                        reactionTime, 
                        VideoPresentationTime, 
                        startRun, 
                        startTrial, 
                        startVid, 0]

            trainingData.iloc[[index]]['Video']
            writerR = csv.writer(ResFile, delimiter=';', lineterminator='\n')
            writerR.writerow(ResEntry)
    except GetOutOfLoop:
        pass
        print('\n\nExperiment aborted.')

    # presenting all actions
    fun.showText(
        win, 'Genoeg geoefend!\n\nDruk op een knop om door te gaan', txtColor, 0.7)
    response = event.waitKeys(keyList=["1", "2", "3", "0"])
    core.wait(2)
    fun.showText(win, """Je krijgt nu van de actie een voorbeeld te zien, met de juiste beschrijving onder in beeld. Let goed op, elke actie komt maar een keer langs. \n\nDruk op een toets om door te gaan.""", txtColor, 0.7)
    core.wait(3)
    response = event.waitKeys(keyList=["1", "2", "3", "0"])
    fun.showText(win, '+', txtColor)
    for v in range(0, len(presVids)):
        ind = VidOrder[v]
        fun.dotVideoText(win, os.path.join(baseDir, "Videos", presVids[ind][1][0], "M1_S3_I0"), os.path.join(
            baseDir, "Images", "Image_Background.png"), 0.04, presVids[ind][2][0], txtColor)
        fun.showText(win, '+', txtColor)
    fun.showText(
        win, 'Genoeg geoefend!\n\n We gaan zo beginnen', txtColor, 0.7)
    response = event.waitKeys(keyList=["0"])


# #############################Testing #############################################

try:
    for run in range(0, 4):  # blocks 1-4 
        trialData = trialData.reindex(np.random.permutation(
            trialData.index))       # reindex --> shuffle
        # waiting for scanner only at the beginning of the block
        launchScan(win, MR_settings, mode=runMode, wait_msg=waitMessage)
        # fun.block_break(win, run, triggerPulse)
        startRun = clock.getTime()
        for trial in range(0, len(trialData)):  # 52 meaningful trials

            startTrial = clock.getTime() - startRun
            pic = os.path.join(baseDir, "Images",
                               trialData.iloc[trial]['ImageFile'])
            vid = os.path.join(
                baseDir, "Videos", trialData.iloc[trial]['VideoFile'], trialData.iloc[trial]['Video'])
            picture = visual.ImageStim(win, pic, pos=(0.0, 0.0))

            # fixation cross
            message = visual.TextStim(
                win, text='+', alignHoriz='center', alignVert='center', color='white', height=1)
            message.draw()
            win.flip()

            # Allow to quit experiment
            response = event.waitKeys(keyList=['q'], maxWait=1.0)
            if response:
                if response[0] == 'q':
                    completed = 0
                    raise GetOutOfLoop

            startVid = clock.getTime() - startRun

            fun.dotVideo1(win, vid, pic, 0.04, .5, 100, 400, dotSize)

            # Greyscreen before answer options
            endVid = clock.getTime() - startRun
            win.flip()
            core.wait(1.5)

            fun.showText(win, '', txtColor)
            VideoPresentationTime = endVid - startVid
            responseStart = clock.getTime()
            fun.showText(win, (trialData.iloc[trial]['Question']+'\n \n 1: ' + trialData.iloc[trial]['Answer1'] +
                               ' 2: '+trialData.iloc[trial]['Answer2']+' 3: '+trialData.iloc[trial]['Answer3']), txtColor)
            response = event.waitKeys(
                keyList=['1', '2', '3', '0', 'q'], maxWait=responseTime)
            reactionTime = clock.getTime()-responseStart
            # conversion into milliseconds
            reactionTime = int(round(reactionTime * 1000))
            # entering results
            ResEntry = [trialData.iloc[trial]['Condition'],
                        trialData.iloc[trial]['Context'],
                        trialData.iloc[trial]['Action'],
                        trialData.iloc[trial]['VideoFile'],
                        trialData.iloc[trial]['NoVideoFile'],
                        trialData.iloc[trial]['ImageFile'],
                        trialData.iloc[trial]['NoImageFile'],
                        trialData.iloc[trial]['TrialNum'],
                        trialData.iloc[trial]['Video'],
                        trialData.iloc[trial]['Video_Marker'],
                        trialData.iloc[trial]['Video_Size'],
                        trialData.iloc[trial]['Video_Mirror'],
                        response[0],
                        reactionTime,
                        VideoPresentationTime,
                        startRun,
                        "{:.3F}".format(startTrial),
                        startVid,
                        run+1]
            writerR = csv.writer(ResFile, delimiter=';', lineterminator='\n')
            writerR.writerow(ResEntry)

        fun.block_break(win, run, triggerPulse)

except GetOutOfLoop:
    pass
    print('\n\nExperiment aborted.')

ResFile.close

# Date and Time (from OS)
DateTimeFinished = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
logEntry = [ParticipantNumber, DateTime, DateTimeFinished, Experimenter]
logFile = open('scan_log.csv', 'a')
writerL = csv.writer(logFile, delimiter=';', lineterminator='\n')
writerL.writerow(logEntry)
logFile.close

core.wait(5)
core.quit()
