# Expriment

This repository contains scripts to run the experiments for the behavioural and fMRI experiments as schemetically illustrated in the following figures. It also includes the stimuli-setups, context-images and motion-capture-based image sequences, used to generate point-cloud-animations on the fly.

## Behavioural Experiment Design

![Behavioural Design](./doc_imgs/TaskDesign_LQ.png)
## fMRI Experiment Design

![FMRI Design](./doc_imgs/fMRI_DesignLQ.png)

---

### Technical Notes
The presentation of stimuli and collection of data was realised in python, using the psychopy toolbox[1]. To run the scripts the following requirements should be met (possibly other configurations/versions work, but the ones given here were tested, running on Linux Ubuntu 18.04):

 - python 2.7
 - psychopy 1.85.3
 - pyglet 1.3.0

 To install the packages using pip, run:
 ```
 pip install pyglet==1.3.0
 pip install psychopy==1.85.3
 ```

 [1] **Peirce, J., Gray, J. R., Simpson, S., MacAskill, M., Richard Höchenberger, Sogo, H., … Jonas Kristoffer Lindelov. (2019).** _PsychoPy2: Experiments in behavior made easy._ Behavior Research Methods, 51(1), 195–203. https://doi.org/10.3758/s13428-018-01193-y