%% IMPORT DATA in Struct %%%%%
function Participant = import_participant_data(csv_folder, b_row_label)
    dirData = dir([csv_folder filesep '*.csv']);
    
    if nargin < 2
        b_row_label = false;
    end
        
    for ix = 1:numel(dirData)
        filename = [dirData(ix).folder filesep dirData(ix).name];
        opts = detectImportOptions(filename,'NumHeaderLines',0, 'ReadRowNames',b_row_label);
        Participant(ix).data = readtable(filename,opts);
        Participant(ix).id = get_participant_number(dirData(ix).name);
    end

end

function id = get_participant_number(filename)
    reg_exp = '[0-9]+';
    match =  regexp(filename,reg_exp,'match');
    id = str2num(cell2mat(match));
end