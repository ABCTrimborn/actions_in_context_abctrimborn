% Plots the accuracies (or errors) for each action in each condition in a 
% seperate subplot for every action to visualize the difficulty of the
% tasks and the differnce of results in the differnt conditions

clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
participant_data = import_participant_data(['..' filesep 'Data']);

n_parti = numel(participant_data);
 
for condition = 1:3
    for parti = 1:n_parti % Loop over Participant datasets
        % Drop non-neutral context and test-actions
        b_test = participant_data(parti).data.NumberVideoFile(:)>20;
        b_cond = participant_data(parti).data.Condition(:) == condition;
        P_dat = participant_data(parti).data(~b_test & b_cond,:); 
        response(parti, condition, :) = P_dat.Response(:);
    end
end

rel_freq_cond = rel_frequencies_contexts(response,2);
[p, table] = anova_rm(rel_freq_cond);
plot_accuracies(rel_freq_cond)

function plot_accuracies(rel_freq_cond)
    figure(2); clf;
    col = [45 157 146]./255;
    bar(1:3, mean(rel_freq_cond,1),'FaceColor', col);
    hold on;
    errorbar(1:3, mean(rel_freq_cond,1),std(rel_freq_cond,[],1),'Color','k','LineStyle','none')
    xticks(1:3)
    xlim([0.5 3.5]);
    xticklabels({'Congruent','Incongruent','Neutral'})
    ylabel('Accuracy')
    ylim([0.4 1]);
    hold off;
    set(gcf,'Position',[10 10 600 300]);  
        
end

function rf = rel_frequencies_contexts(response, result_type)
% This function returns the relative frequencies of correct answers for 
% each context.
%
% Input:
% response: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% Relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_condition, n_trial] = size(response);
    rf = zeros(n_parti, n_condition);
    for parti = 1:n_parti
        for condition = 1:n_condition
            vec_ans = squeeze(response(parti, condition, :));
            rf(parti, condition) = sum(vec_ans(:) == result_type) / n_trial;
        end
    end
end