% Plots the accuracies (or errors) for each action in each condition in a 
% seperate subplot for every action to visualize the difficulty of the
% tasks and the differnce of results in the differnt conditions

clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
participant_data = import_participant_data(['..' filesep 'Data']);

n_parti = numel(participant_data);
 
for block = 1:4
    for parti = 1:n_parti % Loop over Participant datasets
        % Select block entries
%         P_dat = participant_data(parti).data(block_range(block),:).Response(:); 
        response(parti, block, :) = participant_data(parti).data(block_range(block),:).Response(:); 
    end
end

rel_freq_cond = rel_frequencies_contexts(response,2);
[p, table] = anova_rm(rel_freq_cond);
plot_accuracies(rel_freq_cond)

function blk_rng = block_range(block)
    switch block
        case 1
            blk_rng = 13:84;
        case 2
            blk_rng = 85:156;
        case 3
            blk_rng = 157:228;
        case 4
            blk_rng = 229:300;
    end
end
function plot_accuracies(rel_freq_cond)
    figure(1); clf;
    col = [45 157 146]./255;
    bar(1:4, mean(rel_freq_cond,1),'FaceColor', col);
    hold on;
    errorbar(1:4, mean(rel_freq_cond,1),std(rel_freq_cond,[],1),'Color','k','LineStyle','none')
    xticks(1:4)
    xlim([0.5 4.5]);
    xticklabels({'Block 1','Block 2','Block 3', 'Block 4'})
    ylabel('Accuracy')
    ylim([0.4 1]);
%     grid on;
    hold off;
    set(gcf,'Position',[10 10 600 300]);     
end

function rf = rel_frequencies_contexts(response, result_type)
% This function returns the relative frequencies of correct answers for 
% each context.
%
% Input:
% response: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% Relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_condition, n_trial] = size(response);
    rf = zeros(n_parti, n_condition);
    for parti = 1:n_parti
        for condition = 1:n_condition
            vec_ans = squeeze(response(parti, condition, :));
            rf(parti, condition) = sum(vec_ans(:) == result_type) / n_trial;
        end
    end
end