% Plots the accuracies (or errors) for each action in each condition in a 
% seperate subplot for every action to visualize the difficulty of the
% tasks and the differnce of results in the differnt conditions

clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
participant_data = import_participant_data(['..' filesep 'Data']);
action_keys = struct2array(load([fcn_path filesep 'action_keys.mat']));


figure(1); clf;
hold on;
rf = zeros(12,3);

for condition = 1:3
    n_parti = numel(participant_data);
    n_actions = sum(unique(participant_data(1).data.NumberVideoFile(:))<20); % Indices > 20 were test actions
    response =[];
    for parti = 1:n_parti % Loop over Participant datasets
        % Drop non-neutral context and test-actions
        b_test = participant_data(parti).data.NumberVideoFile(:)>20;
        b_neutral = participant_data(parti).data.Condition(:) == condition;
        P_dat = participant_data(parti).data(~b_test & b_neutral,:); 
        n_trials = size(P_dat,1);
        for action = 1:n_actions % Loop over trials
            b_action = P_dat.NumberVideoFile(:) == action;    
            response(parti, action, :) = P_dat.Response(b_action);
        end
    end

    rf(:,condition) = rel_frequencies_actions(response, 2);
    
end

plot_actions_tiled(rf, action_keys)


function plot_actions_tiled(rf_data, action_keys)
figure(2); clf;
tiledlayout('flow','TileSpacing','Compact','Padding','None')
    for ix = 1:size(rf_data,1)
        nexttile
        bar(rf_data(ix,:),'FaceColor','#2d9d92')
        ylim([0.3, 0.9]);
        xticklabels({'Congruent','Incongruent','Neutral'})
        ylabel('Accuracy')
        grid on;
        title(action_keys(ix).description)
    end
end


function rf = rel_frequencies_actions(response, result_type)
% This function returns the relative frequencies of correct answers for 
% each action.
%
% Input:
% response: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% Relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_action, n_trial] = size(response);
    rf = zeros(n_action, 1);
    for action = 1:n_action
        mat_ans = squeeze(response(:, action, :));
        rf(action) = sum(mat_ans(:) == result_type) / (n_parti * n_trial);
    end
end