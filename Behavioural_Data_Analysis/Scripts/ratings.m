% Plots the accuracies (or errors) for each action in each condition in a 
% seperate subplot for every action to visualize the difficulty of the
% tasks and the differnce of results in the differnt conditions

clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
ratings_path = ['..' filesep 'Data' filesep 'Rating'];
participant_data = import_participant_data(ratings_path, true);

[n_contexts, n_actions] = size(participant_data(1).data);
n_parti = numel(participant_data);

response = zeros(n_contexts, n_actions, n_parti);
for parti = 1:n_parti % Loop over Participant datasets
    response(:,:,parti) = table2array(participant_data(parti).data);
end

mean_response = mean(response,3);
std_response = std(response,[],3);

y_label = fix_xy_label(participant_data(1).data.Properties.RowNames, false);
y_order = [1 3 2 4];

x_label = fix_xy_label(participant_data(1).data.Properties.VariableNames, true);
x_order = [12 1 8 9 3 7 5 10 2  11 4 6];

heatmap(x_label(x_order), y_label(y_order), mean_response(y_order,x_order));

% tab = array2table(mean_response(y_order,x_order));
[n_y, n_x] = size(mean_response);

c = cell(n_y,n_x);
for y = 1:n_y
    for x = 1:n_x
        c{y,x} = [num2str(mean_response(y,x),2) ' +/- ' num2str(std_response(y,x),2)];
    end
end
tab = cell2table(c(y_order, x_order));
tab.Properties.RowNames = y_label(y_order);
tab.Properties.VariableNames = x_label(x_order);
writetable(tab, 'rating_means.csv', 'WriteRowNames', true)

axs = struct(gca);
cb = axs.Colorbar;
cb.Ticks = [1 6];
cb.TickLabels = {'Very unlikely','Very likely'};


function xy_label = fix_xy_label(xy_label,b_x)
    for x=1:numel(xy_label)
        if b_x
            xy_label{x} = replace(xy_label{x},'Rating',''); 
        else
            xy_label{x} = xy_label{x}(1:end-5); 
        end
    end
end