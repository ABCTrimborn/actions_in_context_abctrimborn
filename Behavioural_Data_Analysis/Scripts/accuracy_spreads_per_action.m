% in figure one: assessment of task difficulty with the correct responses
% per given action --> needs to be transposed for better readibility
% (actions on vertical axis)

%in figure two: answer types per action, weirdly depicted tho, might not
%be necessary
clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
participant_data = import_participant_data(['..' filesep 'Data']);
action_keys = struct2array(load([fcn_path filesep 'action_keys.mat']));

% Initialize figures
figure(1); clf;
figure(2); clf;

for condition = 1:3
    n_parti = numel(participant_data);
    n_actions = sum(unique(participant_data(1).data.NumberVideoFile(:))<20); % Indices > 20 were test actions
    response =[];
    for parti = 1:n_parti % Loop over Participant datasets
        % Drop non-neutral context and test-actions
        b_test = participant_data(parti).data.NumberVideoFile(:)>20;
        b_neutral = participant_data(parti).data.Condition(:) == condition;
        P_dat = participant_data(parti).data(~b_test & b_neutral,:); 
        n_trials = size(P_dat,1);
        for action = 1:n_actions % Loop over trials
            b_action = P_dat.NumberVideoFile(:) == action;    
            response(parti, action, :) = P_dat.Response(b_action);
        end
    end
    
    % Plot errorbars with slight offset in x for better visualisation
    x = (1:n_actions) - (2-condition)/10;
    
    figure(1); hold on;
    [rf_mean, rf_std] = dist_moments_over_participants(response, 2);
    errorbar(x, rf_mean, rf_std,'o')
    
    figure(2); hold on;
    [rf_mean, rf_std] = dist_moments_over_trials(response, 2);
    errorbar(x, rf_mean, rf_std,'o')
    
end

decorate_plot(1, action_keys, 'Accuracies for each action in the 3 differnt conditions and the spread over participants');
decorate_plot(2, action_keys, 'Accuracies for each action in the 3 differnt conditions and the spread over trials');


function decorate_plot(fig, action_keys, title_str)

    figure(fig)
    xlim([0.5 12.5]);
    xticks(1:12);
    xticklabels({action_keys(:).description})
    xtickangle(45)
    xlabel('Action')
    
    ylim([0.1 1.05])
    ylabel('Accuracy')
    grid on;
    legend('Congruent context', 'Incongruent context', 'Neutral context','Location','southeast')
    title(title_str)
end

function [rf_mean, rf_std] = dist_moments_over_participants(response, result_type)
% This function returns the mean and standard deviation over participant
% accuracies. These values essentially state the mean accuracy of the
% participants, e.g. On average the participants got it x% of the time
% right
%
% Input:
% respons: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% First and second Moments of the distributions for relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_action, n_trial] = size(response);
    rf = zeros(n_parti, n_action);
    for parti = 1:n_parti
        for action = 1:n_action
            vect_ans = squeeze(response(parti, action, :));
            rf(parti, action) = sum(vect_ans == result_type) / n_trial;
        end
    end
    rf_mean = mean(rf,1);
    rf_std = std(rf,1);
end

function [rf_mean, rf_std] = dist_moments_over_trials(response, result_type)
% This function returns the mean and standard deviation over trial
% accuracies. These values essentially state the mean accuracy of the
% trials, e.g: On average x% of the participants got it right.
%
% Input:
% respons: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% First and second Moments of the distributions for relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_action, n_trial] = size(response);
    rf = zeros(n_trial, n_action);
    for trial = 1:n_trial
        for action = 1:n_action
            vect_ans = squeeze(response(:, action, trial));
            rf(trial, action) = sum(vect_ans == result_type) / n_parti;
        end
    end
    rf_mean = mean(rf,1);
    rf_std = std(rf,1);
end
