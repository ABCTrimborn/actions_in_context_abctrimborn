% Plots the accuracies (or errors) for each action in each condition in a 
% seperate subplot for every action to visualize the difficulty of the
% tasks and the differnce of results in the differnt conditions

clear; clc;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

%% Import Data %%%%%
participant_data = import_participant_data(['..' filesep 'Data']);

n_parti = numel(participant_data);

for posture = 1:2
    for parti = 1:n_parti % Loop over Participant datasets
        % Select block entries
%         P_dat = participant_data(parti).data(block_range(block),:).Response(:); 
        dat_rng = data_range(posture, participant_data(parti).id);
        response(parti, posture, :) = participant_data(parti).data(dat_rng,:).Response(:); 
    end
end

rel_freq_cond = rel_frequencies_contexts(response,2);
[p, table] = anova_rm(rel_freq_cond);
plot_accuracies(rel_freq_cond)

function dat_rng = data_range(posture, participant_id)
    block1_idx = 13:84;
    block2_idx = 85:156;
    block3_idx = 157:228;
    block4_idx = 229:300;
    participant_posture_group_A =  [1,2,4,6,8,10,13,14,17,19,20,21];
    if ismember(participant_id,participant_posture_group_A)
        switch posture
            case 1
                dat_rng = [block1_idx block3_idx];
            case 2
                dat_rng = [block2_idx block4_idx];
        end
    else
        switch posture
            case 1
                dat_rng = [block2_idx block4_idx];
            case 2
                dat_rng = [block1_idx block3_idx];
        end
    end
end

function plot_accuracies(rel_freq_cond)
    figure(3); clf;
    stderror = std(rel_freq_cond,[],1) / sqrt(size(rel_freq_cond,1));
    errorbar(1:2, mean(rel_freq_cond,1),stderror,'o','Color','#2d9d92','MarkerSize',8, 'MarkerFaceColor','#2d9d92','LineStyle','none')
    xticks(1:2)
    xlim([0.5 2.5]);
    xticklabels({'Wide Posture','Narrow Posture'})
    ylabel('Accuracy')
    grid on;
        
end

function rf = rel_frequencies_contexts(response, result_type)
% This function returns the relative frequencies of correct answers for 
% each context.
%
% Input:
% response: cell array of actions with answer evaluations, with:
% 0 = Wrong answer and wrong context
% 1 = Wrong answer but correct context
% 2 = Correct answer
% 5 = Missing answer
%
% result_type: result type to query with type as noted above
%
% Output:
% Relative frequencies of
% e.g. accuracy or error...
    [n_parti, n_condition, n_trial] = size(response);
    rf = zeros(n_parti, n_condition);
    for parti = 1:n_parti
        for condition = 1:n_condition
            vec_ans = squeeze(response(parti, condition, :));
            rf(parti, condition) = sum(vec_ans(:) == result_type) / n_trial;
        end
    end
end