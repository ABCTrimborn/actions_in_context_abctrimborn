%###########################################################
%% Script setup 
%###########################################################
% free up memory, clear variables
clear; clc; close all;
fcn_path = ['..' filesep 'Functions'];
addpath(fcn_path)

% Want to actually create all the folders and images or just checking and adjusting?
createFiles = true;


data_path = ['..' filesep 'Data'];
% prms contains start and end frames and rotation angle around z for each
% action
load([data_path filesep 'action_parameters.mat'])


% Setup Frame frequency
frameRateCapture    = 100;
frameRateVideo      = 30;

% Get names of all motion-capture-files in folder
motion_capture_files = dir([data_path filesep '*.tsv']);
filenames = cell(numel(motion_capture_files),1);
for f=1:numel(motion_capture_files)
    filenames{f}=motion_capture_files(f).name;
end

% Marker selection for the 4 marker groups
skipMarkersOpt = {...
{},...
{'LFIN1' 'RFIN2' 'RFIN4' 'LFIN4' 'LWRO' 'LFIN2' 'RFIN3' 'LFIN5' 'RWRI' 'RFIN5''LSHO' 'LFHD' 'AERIAL' 'LANK' 'LFWT' 'RBWT' 'LBWT' 'LKNE' 'RTOE' 'MFWT' 'CLAV' 'RFWT'},...
{'LFIN5' 'RFIN4' 'RFIN5' 'RFIN3' 'LFIN4' 'RWRO' 'LFIN1' 'LFIN3' 'LWRI' 'RFIN1''RTOE' 'LTOE' 'RANK' 'LFHD' 'RELB' 'LBWT' 'RBHD' 'STRN' 'C7' 'LSHO' 'LELB' 'MFWT'},...
{'LFIN2' 'RFIN1' 'LWRI' 'RWRI' 'RFIN5' 'RFIN2' 'LFIN3' 'RFIN4' 'LWRO' 'LFIN5''RKNE' 'RBHD' 'CLAV' 'AERIAL' 'STRN' 'LKNE' 'RTOE' 'LTOE' 'RELB' 'RANK' 'T10' 'C7'},...
{'RFIN1' 'RFIN3' 'RFIN2' 'LFIN1' 'RWRO' 'RFIN5' 'LWRO' 'LWRI' 'LFIN4' 'LFIN5''CLAV' 'LBHD' 'RBWT' 'RBHD' 'LELB' 'LFWT' 'LFHD' 'LTOE' 'RELB' 'LKNE' 'RFWT' 'LSHO'}};

%###########################################################
%% Data import from .tsv file 
%###########################################################

for fil=1:numel(filenames)
    tsv_file = [data_path filesep filenames{fil}];
    [pathstr,name,ext] = fileparts(tsv_file);
    
    Marker = get_marker_names(tsv_file);
    DataMatrix = read_motion_capture_data(tsv_file);
    
    %###########################################################
    %% Data preparation and varible initialization 
    %###########################################################

    for i_dat=1:size(prms,1)
        if strcmpi([prms.Name_Action{i_dat} '.tsv'], filenames{fil})
          firstFrame = prms.Frame_Start(i_dat);
          lastFrame = prms.Frame_End(i_dat);
          FigAng = str2num(prms.rotation{i_dat});
        end
    end

    %Initiaialize coordinate vectors
    IntMarker=numel(Marker);
    FiguresToPlot=(lastFrame-firstFrame)*ceil(frameRateVideo/frameRateCapture);
    XVector=zeros(FiguresToPlot,IntMarker);
    YVector=zeros(FiguresToPlot,IntMarker);
    ZVector=zeros(FiguresToPlot,IntMarker);

    %% Create all point vectors first, including all markers and frames, with e.g.:
    % Coordinates = [XVector(Frame#,Marker#) YVector(Frame#,Marker#) ZVector(Frame#,Marker#)]
    FrameCount=0;
    for i_frm=firstFrame:lastFrame
        X=1; Y=1; Z=1; FrameCount=FrameCount+1;
        for i_dat=1:size(DataMatrix,2)%Loop through all data columns and assign values to corresponding coordiante vectors
            rem=mod(i_dat,3); %mod(a,b) returns the remainder of division a/b
            if rem==2           %Check if its X,Y or Z Column
               XVector(FrameCount,X) = DataMatrix(i_frm,i_dat);
               X=X+1;
            elseif rem==1      %Check if its X,Y or Z Column
               YVector(FrameCount,Y) = DataMatrix(i_frm,i_dat);
               Y=Y+1;
            elseif rem==0       %Check if its X,Y or Z Column
               ZVector(FrameCount,Z) = DataMatrix(i_frm,i_dat);
               Z=Z+1;
            end
        end
    end      


    %Initialize empty 3D plot with figure handle h
    h = plot_3d_cloud(XVector(1,:), YVector(1,:), ZVector(1,:), FigAng);

    %###########################################################
    %% Main nested loop through all possibilities of one action 
    %###########################################################

    for invert=0:1  % inverted: 0=no; 1=yes
        for mannequinSize = 1:3 % each size from small to large
            for markerSetNumber = 1:5 % each set of Markers
                % determine skipIndex for choice of markers
                skipMarkers = skipMarkersOpt{markerSetNumber};
                skipIndex = ~ismember(Marker, skipMarkers); 

                % set size    
                switch(mannequinSize)
                    case 1 
                        Scale=0.5;   
                    case 2
                        Scale=0.75; 
                    case 3 
                        Scale=1; 
                end

                % Set Vectors to plot
                z_max = max(DataMatrix(:));
                ZShift =(z_max - z_max * Scale)/2;
                XVectorP=XVector(:,skipIndex) * Scale;
                YVectorP=YVector(:,skipIndex) * Scale;
                ZVectorP=(ZVector(:,skipIndex) * Scale) + ZShift;
                % Set missing values to nan to avoid plotting them at (0,0,0)
                XVectorP(XVectorP==0)=nan; YVectorP(YVectorP==0)=nan; ZVectorP(ZVectorP==0)=nan;

                % invert Vectors to plot    
                if invert == 1
                  XVectorP=XVectorP+1000; XVectorP=XVectorP*-1; XVectorP=XVectorP+1000;       
                end 

                % create individual folder for each of the 24 possibilities in the folder named
                % according to the tsv file, with nomenclature:
                % M=MarkerSetNumber, S=Sizenumber, I=Inversion 
                if createFiles==true
                  PicPath = ['..' filesep 'Output' filesep name filesep 'M' num2str(markerSetNumber) '_S' num2str(mannequinSize) '_I' num2str(invert)];
                  [~,~] = mkdir(PicPath);
                end

                % Loop through the frames and save images in respective folders 
                for FrameCount=1:ceil(frameRateCapture/frameRateVideo):(lastFrame-firstFrame)
                    % get Linehandle of the current plot (the content of the figure)
                    LineHandle = findobj(h, 'type', 'line');
                    % swap Data with updated vectors
                    set(LineHandle,'XData',(XVectorP(FrameCount,:)),'YData',(YVectorP(FrameCount,:)), 'ZData',(ZVectorP(FrameCount,:))); 
                    drawnow 
                    if createFiles==true
                        save_path = [PicPath filesep num2str(FrameCount-1) '.png'];
                        img = print(h,'-RGBImage','-r300');
                        alpha = uint8(~all(img == 255, 3).*255);
                        imwrite(img, save_path, 'Alpha', alpha);
                    end  
                end
            end
        end
    end
end