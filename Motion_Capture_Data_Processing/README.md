# Motion Capture Data Processing

This repository contains motion capture data and a matlab script to generate point-cloud visualisations. 
![Hammering](imgs/hammering.gif)
The visualisations include differnt settings:
* 3 Scaling steps (to change the relative size of the figure in the images)
* 2 Rotations of the figure (the points x-coordinates are inverted)
* 4 Marker subsets