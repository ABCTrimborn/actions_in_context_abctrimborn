function fig=plot_3d_cloud(X, Y, Z, R)
%plot_3d_cloud(X1, Y1, Z1)
%  X:  vector of x data
%  Y:  vector of y data
%  Z:  vector of z data
%  R:  Rotation vector, e.g [-105 0]

% Create figure
fig = figure(1);

% Create axes
axes1 = axes('Visible','off','Parent',fig,'DataAspectRatio',[1 1 1]);

% Set view
view(axes1, R);

% Create plot3
hold(axes1,'all');
plot3(X,Y,Z,'MarkerSize',5,'Marker','o','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'LineStyle','none',...
    'Color',[0.99 0.99 0.99]);
axis equal                           %avoid rescaling/stretching
axis([-500 500 -400 400 0 1800])     %set frame dimensions in millimetre
    
set(fig,'units','pixel', 'Position', [10, -10, 900, 900], 'menubar', 'none');  %set figure size and position   
    

